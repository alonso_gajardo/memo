/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "Particle.h"
#line 1 "/home/alonsgarpar/Escritorio/Particle/AG247/memo/memoriafeather/src/memoriafeather.ino"
#include "AssetTrackerRK.h"
#include "LIS3DH.h"
#include "eEPROM_I2C.h"

void setupGpsFeatherAg();
bool setupAcelIdc();
void movementInterruptHandler();
void parsingLiveMemo(int vectorPosicion);
void parsingMemoLive(int vectorPosicion);
void updateTimeForPublish();
bool publishSensor();
void setup();
void loop();
#line 5 "/home/alonsgarpar/Escritorio/Particle/AG247/memo/memoriafeather/src/memoriafeather.ino"
#define idmsg "testing"

SYSTEM_MODE(MANUAL);
//SYSTEM_THREAD(ENABLED);

struct estructuraDeDatosDeMemoria
{
  int sizeStruct = 50;  //2bytes
  long unixtimeint = 0; //4 bytes
  long Counter = 0;     //4 bytes
  float lat = 0.00000;  //4 bytes
  float lon = 0.00000;  //4 bytes
  float bat = 0.00000;  //4 bytes
  float altura = 0.00000;
  float otrodato = 0.00000;
  float otrodatodos = 0.00000;
  float openBox = 0.00000; //4 bytes
  uint8_t correct_init = false;
  uint8_t correct_init2 = false;
  //total = 3* = 78 bytes total
} estructuraMemoria;

struct estructuraDeDatosPrincipal
{
  volatile float altura = 0.00000;   //4 bytes 64bits
  volatile float otrodato = 0.00000; //4 bytes 64bits
  volatile float otrodatodos = 0.00000;
  volatile float openBox = 0.00000;
  volatile uint8_t correct_init = false;
  volatile uint8_t correct_init2 = false;
  long counter_to_sleep = 0;
  long Counter = 0;
  long err;
  long unixtimeint;
  int cantidadNodoEnCola = 0;
  int inic = 0;
  unsigned int whocharge;
  float bat = 0.00000;
  float lat = 0.00000;
  float lon = 0.00000;
  String timestamp;
  String unixtime;
  String msg = "Conectado";
  String id = "";

} estructuraPrincipal;

static char s = '"';
volatile bool movementInterrupt = false; //sera false si esta quieto
enum State
{
  INICIALIZADOR,
  LOOP,
  LOGS,
  RECUPERADOR
};
State state = INICIALIZADOR;

static unsigned long timeloop = 1000 * 60 * 0; // 10 segundos
static unsigned long crono = 0;

ApplicationWatchdog *wd;
FuelGauge fuel;
AssetTracker gps;
eEPROM_I2C memo;
LIS3DHI2C accel(Wire, 0, D8);

template <class T>
int EEPROM_writeAnything(int ee, const T &value)
{
  const byte *p = (const byte *)(const void *)&value;
  unsigned int i;
  for (i = 0; i < sizeof(value); i++)
    memo.write8(ee++, *p++);
  return i;
}

template <class T>
int EEPROM_readAnything(int ee, T &value)
{
  byte *p = (byte *)(void *)&value;
  unsigned int i;
  for (i = 0; i < sizeof(value); i++)
    *p++ = memo.read8(ee++);

  return i;
}


void setupGpsFeatherAg()
{
  gps.withI2C();
  gps.startThreadedMode();
  int c = 0;
  while (!gps.gpsFix() && c < 5)
  {
    c++;
    Serial.print("20");
    Serial.print(gps.getYear());
    Serial.print("-");
    if (gps.getMonth() < 10)
    {
      Serial.print("0");
    }
    Serial.print(gps.getMonth());
    Serial.print("-");
    if (gps.getDay() < 10)
    {
      Serial.print("0");
    }
    Serial.print(gps.getDay());
    Serial.print("T");
    if (gps.getHour() - 3 < 10)
    {
      Serial.print("0");
    }
    Serial.print(gps.getHour() - 3);
    Serial.print(":");
    if (gps.getMinute() < 10)
    {
      Serial.print("0");
    }
    Serial.print(gps.getMinute());
    Serial.print(":");
    if (gps.getSeconds() < 10)
    {
      Serial.print("0");
    }
    Serial.print(gps.getSeconds());
    Serial.println("Z");
    delay(1000);
  }
  int diasDesde2021de01 = (gps.getMonth() - 1) * 30 + gps.getDay();
  int segundosDeLosDiasDesdeElPrincipioDeAnio = ((diasDesde2021de01 * 24 + gps.getHour()) * 60 + gps.getMinute()) * 60 + gps.getSeconds();
  Time.setTime(segundosDeLosDiasDesdeElPrincipioDeAnio + 1609459200 - 60 * 60 * 3);
}

bool setupAcelIdc()
{
  Wire.setSpeed(CLOCK_SPEED_100KHZ);
  Wire.begin();
  // Initialize sensor
  LIS3DHConfig config;
  config.setAccelMode(LIS3DH::RATE_200_HZ);
  config.setLowPowerWakeMode(4);
  bool setupSuccess = accel.setup(config);
  attachInterrupt(D8, movementInterruptHandler, CHANGE);
  return setupSuccess;
}

void movementInterruptHandler()
{
  Serial.println("vbr");
  movementInterrupt = true;
}

//escribir la estruct en la memoria
void parsingLiveMemo(int vectorPosicion)
{
  if (estructuraPrincipal.correct_init)
  {
    estructuraDeDatosDeMemoria myObj = {42, estructuraPrincipal.unixtimeint, estructuraPrincipal.Counter, estructuraPrincipal.lat, estructuraPrincipal.lon, estructuraPrincipal.bat, estructuraPrincipal.altura, estructuraPrincipal.otrodato, estructuraPrincipal.otrodatodos, estructuraPrincipal.openBox, estructuraPrincipal.correct_init, estructuraPrincipal.correct_init2};
    Serial.print(" ESCRIBIENDO EN LA POSICION ");
    Serial.println(vectorPosicion);
    EEPROM_writeAnything(vectorPosicion, myObj);
  }
}

//recuperar la struct de la memoria!
void parsingMemoLive(int vectorPosicion)
{
  if (estructuraPrincipal.correct_init)
  {
    Serial.print(" RECUPERANDO EN LA POSICION ");
    Serial.println(vectorPosicion);
    EEPROM_readAnything(vectorPosicion, estructuraMemoria);
  }
}

void updateTimeForPublish()
{
  estructuraPrincipal.timestamp = Time.format(TIME_FORMAT_ISO8601_FULL);
  estructuraPrincipal.unixtimeint = (long int)Time.now();
  char data2[80];
  snprintf(data2, sizeof(data2), "%lu000000000", estructuraPrincipal.unixtimeint);
  estructuraPrincipal.unixtime = data2;
}

bool publishSensor()
{
  Serial.println("PUBLISH");
  //publica en nube la informacion sensada, de lo contrario va guardando en cola en la memoria el dato que no se puede enviar.
  char data[380];
  snprintf(data, sizeof(data), "{ \"tags\" : {\"id\":%c%s%c}, \"values\": {\"t\":%s,\"ht\":%c%s%c,\"bt\":%0.2f,\"c\":%li,\"wh\":%i,\"lt\":%0.4f,\"ln\":%0.4f,\"altura\":%0.1f,\"otrodato\":%0.1f,\"otrodatodos\":%0.1f,\"openbox\":%0.1f}}", s, idmsg, s, estructuraPrincipal.unixtime.c_str(), s, estructuraPrincipal.timestamp.c_str(), s, estructuraPrincipal.bat, estructuraPrincipal.Counter, estructuraPrincipal.whocharge, estructuraPrincipal.lat, estructuraPrincipal.lon, estructuraPrincipal.altura, estructuraPrincipal.otrodato, estructuraPrincipal.otrodatodos, estructuraPrincipal.openBox);
  Serial.println(data);
  if (Cellular.ready())
  {
    Serial.println("Publicando");
    return true;
  }
  else
  {
    //if (recuperador)
    //{
    //  ;
    //}
    //else
    //{
    //  //cuando provenga el estado de cantidadcola>5, se resetea este valor a cantidadNodoCola=0
    //  estructuraPrincipal.cantidadNodoEnCola++; //1,2,3,4,5,6
    //  Serial.print(" CANTIDAD EN MEMORIA LA COLA ");
    //  Serial.print(estructuraPrincipal.cantidadNodoEnCola);
    //  Serial.print(" UBICACION EN MEMORIA LA COLA "); //100,150,200,250,300,350
    //  //entonces cuanto tiene que ser el valor de este numero cuando la cantidad es 0?->100
    //  Serial.println(50 + (estructuraPrincipal.cantidadNodoEnCola * 50));
    //
    //  parsingLiveMemo(50 + (estructuraPrincipal.cantidadNodoEnCola * 50));
    //  //parsingMemoLive(50 + (estructuraPrincipal.cantidadNodoEnCola * 50));
    //}
    //Serial.println("finPUBLISH");
    return false;
  }
}





void setup()
{
  Cellular.off();
  Serial.begin(9600);
  delay(4000);
  crono = millis();
}

void loop()
{
  accel.clearInterrupt();
  if (millis() - crono > timeloop)
  {
    switch (state)
    {
    case INICIALIZADOR:
    {
      Serial.println("INICIALIZADOR");
      //encendemos placa
      pinMode(D6, OUTPUT);
      digitalWrite(D6, HIGH);
      pinMode(D0, INPUT_PULLUP);
      pinMode(D1, INPUT_PULLUP);
      //configuramos placa
      setupGpsFeatherAg();
      estructuraPrincipal.correct_init = setupAcelIdc();
      estructuraPrincipal.correct_init = memo.begin() && estructuraPrincipal.correct_init;

      //si placa ok consultamos veces de inicio
      if (estructuraPrincipal.correct_init)
      {
        estructuraPrincipal.inic = memo.read8(0); //LEER PRIMERA POSICION DE MEMORIA
        memo.write8(0, estructuraPrincipal.inic + 1);
      }
      state = LOOP;
      break;
    }
    case LOOP:
    {
      Serial.println("LOOP");
      //no hay internet, la cola tiene cantidad 1,
      timeloop = 1000 * 1;
      if (estructuraPrincipal.cantidadNodoEnCola == 0)
      {
        //es el primer dato
        parsingMemoLive(50 + (estructuraPrincipal.cantidadNodoEnCola * 50));
      }
      else
      {

        //dato anterior en caso que se requiera la cola.
        //si cola cantidad es >0 en este caso 1, la consulta sera al velor de 50, ya que es el dato anterior !/.

        parsingMemoLive(50 + ((estructuraPrincipal.cantidadNodoEnCola - 1) * 50)); //recuperamos la estructura de memoria ubicada en la posicion 50
      }
      estructuraPrincipal.Counter++;
      estructuraPrincipal.lat = -32.21231;
      estructuraPrincipal.lon = -69.23121;
      estructuraPrincipal.openBox = -123.00000;
      estructuraPrincipal.altura = 523.12321;
      estructuraPrincipal.otrodato = 1223.21212;
      estructuraPrincipal.otrodatodos = 3923.19201;
      estructuraPrincipal.bat = 84.23;
      estructuraPrincipal.whocharge = 2;
      updateTimeForPublish();

      bool publicacionEfectiva = publishSensor(); //si realmente no lo publica el contador aumenta y grabara en 100 y nunca en 50!
      //en ese caso guardara el dato nuevo 1 que siempre lo va a hacer
      parsingLiveMemo(50 + (estructuraPrincipal.cantidadNodoEnCola * 50)); //siempre los guardamos en la ubicacion que se consulto!

      //vuelve a comprobar exito de envio
      if (publicacionEfectiva)
      {
        //no activamos la cola
        estructuraPrincipal.cantidadNodoEnCola = 0;
      }
      else
      {
        //activamos la cola y comienza en 1
        estructuraPrincipal.cantidadNodoEnCola++;
      }

      state = LOGS;
      if (estructuraPrincipal.cantidadNodoEnCola > 9)
      {
        state = RECUPERADOR;
      }
      break;
    }
    case LOGS:
    {
      if (movementInterrupt)
      {
        movementInterrupt = false;
      }
      timeloop = 1000 * 5 * 0;
      state = LOOP;
      break;
    }
    case RECUPERADOR:
    {
      Serial.println("RECUPERADOR");

      //debemos escribir la logica de recuperacion de red,
      if (true /*SE PUDO VOLVER A CONECTAR A LA NUBE*/)
      {
        int aux = 50;
        for (int i = 0; i < estructuraPrincipal.cantidadNodoEnCola; i++)
        {
          Serial.print("RECUPERANDO UBICACION : ");Serial.println(aux);
          parsingMemoLive(aux); //recuperamos informacion de envio esta vez 50!
          aux = aux + 50;
          Serial.print(" unixtimeint ");
          Serial.println(estructuraMemoria.unixtimeint);
          Serial.print(" bat ");
          Serial.println(estructuraMemoria.bat);
          Serial.print(" Counter ");
          Serial.println(estructuraMemoria.Counter);
          Serial.print(" lat ");
          Serial.println(estructuraMemoria.lat);
          Serial.print(" lon ");
          Serial.println(estructuraMemoria.lon);
          Serial.print(" altura ");
          Serial.println(estructuraMemoria.altura);
          Serial.print(" otrodato ");
          Serial.println(estructuraMemoria.otrodato);
          Serial.print(" otrodatodos ");
          Serial.println(estructuraMemoria.otrodatodos);
          Serial.print(" openBox ");
          Serial.println(estructuraMemoria.openBox);
          Serial.print(" correct_init ");
          Serial.println(estructuraMemoria.correct_init);
          Serial.print(" correct_init2 ");
          Serial.println(estructuraMemoria.correct_init2);
          estructuraPrincipal.unixtimeint = estructuraMemoria.unixtimeint;
          char data2[80];
          snprintf(data2, sizeof(data2), "%lu000000000", estructuraPrincipal.unixtimeint);
          estructuraPrincipal.unixtime = data2;
          estructuraPrincipal.bat = estructuraMemoria.bat;
          estructuraPrincipal.Counter = estructuraMemoria.Counter;
          estructuraPrincipal.lat = estructuraMemoria.lat;
          estructuraPrincipal.lon = estructuraMemoria.lon;
          estructuraPrincipal.altura = estructuraMemoria.altura;
          estructuraPrincipal.otrodato = estructuraMemoria.otrodato;
          estructuraPrincipal.otrodatodos = estructuraMemoria.otrodatodos;
          estructuraPrincipal.openBox = estructuraMemoria.openBox;
          estructuraPrincipal.correct_init = estructuraMemoria.correct_init;
          estructuraPrincipal.correct_init2 = estructuraMemoria.correct_init2;
          publishSensor();
          delay(2000);
        }
      }
      estructuraPrincipal.cantidadNodoEnCola = 0;
      state = LOGS;
      break;
    }
    }
    
    crono = millis();
  }
}

